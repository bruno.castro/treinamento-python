# import copy
#
# empresa = 'HBSIS'
# # lista_hbsis = [item for item in 'HBSIS']
# tpl = dict()
# tpl = dict(nome = 'hbsis', curso = 'python')
# print(tpl)
# # dict01 = dict({'nome' : 'bruno'})
# tpl.update(cidade = 'blumenau')
# print(tpl)
# tpl.popitem() #.popitem remove o ultimo
# print(tpl)
# tpl.copy()
# # print(dict01)
# # print(lista_hbsis)
#
# nome = 'ambev'
#
# def hello() -> str:
#     global nome
#     print(nome)
#     nome = 'hbsis-ambev'
#     return nome
#
# print(hello())

########## *ARGS e **KWARGS
# def soma(mensagem, verbose=None, *args):
#     print(mensagem)
#     retorno = 0
#     for numero in args:
#         retorno += numero
#
#     return retorno
# print(soma(1,2,3,4,5,6))

# def func01(**kwargs):
#     for key, value in kwargs.items():
#         print(f'Argumento: {key} | Valor: {value}')
#
# func01(nome='hbsis', cidade='blumenau', curson='python')
#
# lista_valores = ['Curso Python', 'Blumenau']
#
# def info(nome_curso, cidade):
#     return f'{nome_curso} - {cidade}'
#
# print(info(*lista_valores))
#
# dict_valores = {'nome': 'Python', 'cidade': 'Blumenau', 'empresa': 'hbsis'}
# def info(nome, cidade, empresa, args01=None):
#     return f'Curso: {nome}, Cidade: {cidade}, Empresa: {empresa}'
#
# print(info(**dict_valores))

########Função Recursiva
# def factorial(n):
#     if n == 1:
#         return 1
#     else:
#         return n * factorial(n-1)
#
# print(factorial(4))

###########Funções anonimas, são utilizada para não precisarmos
# criar uma função para algo especifico.

# def quadrado():
#     return 2 ** 2
#
# print(quadrado())

# quadrado = lambda: 2 ** 2    ### <- Declarando função Anônima,
# #
#
# print(quadrado())

### map
# items = [1,2,3,4,5]
# squared = list(map(lambda x: x**2, items)) Essa linha equivale a função logo abaixo

# items = [1,2,3,4,5]
# squared = []
# for i in items:
#     squared.append(i**2)

# print(squared)

### filter percorre item por item aplicando a função


print('Operador.py')