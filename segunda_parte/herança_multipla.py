class A:
    def imprime_A(self):
        return "A"


class B:
    def imprime_B(self):
        return 'B'


class C(A, B):
    pass


c = C()

print(c.imprime_A())
print(c.imprime_B())