from abc import ABC, abstractmethod

class ReprMixin:
    def __repr__(self):
        return f'<{self.__class__.__name__}'


class Employee(ABC):
    def __init__(self, name: str, salary: float):
        super().__init__()
        self._name = name
        self._salary = salary

    @abstractmethod
    def calculate_salary(self):
        raise NotImplementedError

    @property
    def salary(self):
        return self._salary


class ComissionedEmployee(Employee):
    def __init__(self, name, salary, value, comission):
        super().__init__(name, salary)
        self._value = value
        self._comission = comission

    def calculate_salary(self):
        return self._salary + (self._value * (self._comission / 100))

#
# Alex = Employee('Alex', 1000.01)
# print(Alex.calculate_salary())

ambev = ComissionedEmployee('Ambev', 600.00, 1000, 10)
print(ambev.calculate_salary())